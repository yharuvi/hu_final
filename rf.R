library(tidyverse)
library(magrittr)
library(randomForestSRC)


train_surv_rf <- function(df, vars, ntree = 50, mtry = 10, ...) {
  tmp_df <- df %>%
    select(time, status, all_of({{vars}})) %>%
    mutate(status = status %>% as.numeric()) %>% 
    as.data.frame()
  
  rf <- rfsrc(Surv(time, status) ~ ., data = tmp_df,
              splitrule = "logrank",
              ntree = ntree,
              mtry = mtry,
              block.size = 1, ...)
  return(rf)
}

plot_vimp <- function(importance, nrisk = 10, nprotect = 10) {
  # importance is just a named numeric: variable names and scores
  imp_tbl <- tibble(
    var_name = importance %>% names(),
    imp = importance,
  ) %>% mutate(
    rnk = rank(imp)
  ) %>% 
    arrange(desc(rnk))
  
  bind_rows(
    imp_tbl %>% head(nrisk) %>% mutate(affect = "risk"),
    imp_tbl %>% tail(nprotect) %>% mutate(affect = "protective")
  ) %>% 
    mutate(var_name = factor(var_name, levels = unique(var_name))) %>% 
    ggplot(aes(var_name, imp, fill = affect)) +
    geom_bar(stat = "identity") +
    coord_flip() + 
    labs(title = " Random Forest - Variable Importance", y = "importance", 
         x = element_blank()) +
    theme_bw()
}

# # usage example
# vars <- c(clinical_vars, gene_vars, mutations_vars)
# rf_obj <- train_surv_rf(proc, vars, nsplit = 5)
# print(rf_obj)
# plot(rf_obj)
# imp <- vimp(rf_obj)
# plot(imp)
