library(sparcl)
library(zeallot)
library(survival)
library(survminer)

source("read_data.R")
data <- read_data()

source("preprocess.R")
proc <- data %>% preprocess()


train_sparse_kmeans <- function(x, k, seed = 123, wbounds) {
  set.seed(seed)
  if (length(wbounds) > 1) {
    km_perm <- KMeansSparseCluster.permute(x, k, wbounds = wbounds, nperms = 5)
    km_out <- KMeansSparseCluster(x, k, wbounds=km_perm$bestw)
    return(list(km_perm = km_perm, km_out = km_out))
  } else if (length(wbounds == 1)) {
    km_out <- KMeansSparseCluster(x, k, wbounds=wbounds)
    return(list(km_perm = NA, km_out = km_out))
  }
}

save_clusters_to_file <- function(patient_ids, km_obj, file_name) {
  tibble(
    patient_id = patient_ids,
    km_cluster = km_out[[1]]$Cs
  ) %>% write_csv(paste0("./data/", file_name, ".csv"))
}


plot_km_by_cluster <- function(df, km_obj) {
  df_tmp <- df %>% 
    select(time, status) %>% 
    mutate(clust = km_obj[[1]]$Cs %>% as_factor())
  
  ggsurvplot(fit = survfit(Surv(time, status) ~ clust, df_tmp),
             data = df_tmp)
}


extract_prominent_genes <- function(km_obj, threshold) {
  km_obj[[1]]$ws %>% keep(~ .x > threshold) %>% sort(decreasing = TRUE)
}

# 3-class clustering--------------------------------------------------------

genes <- proc %>% select(all_of(gene_vars)) %>% as.matrix()
genes %>% dim()

# search for an optimal tuning-parameter and train for it:
# This will take long-time to train
c(km_perm, km_out) %<-% train_sparse_kmeans(genes, k = 3,
                                            wbounds = seq(10, 19, len = 25))

# or: train for a known value of the tuning parameter 
# (km_perm will get NA for this case)
# (optimal bound is about 14.9, depends on the seed)

c(km_perm, km_out) %<-% train_sparse_kmeans(genes, k = 3, wbounds = 14.9)

# save to file
save_clusters_to_file(proc$patient_id, km_out, "my_kmeans_clusters")

# plot km curves by clusters
plot_km_by_cluster(proc, km_out)

# extract genes with highest weights 
extract_prominent_genes(km_out, threshold = 0.08)




